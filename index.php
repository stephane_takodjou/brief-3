<!DOCTYPE html>
<html>
<head>
	<title>formulaire d'inscription</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-offset-3 col-md-6 form_content">
				<form method="post" action="traitement.php" enctype="multipart/form-data">
					<h1>inscription</h1>
						<div class="form-group ">
							<label  for="nom">Nom:</label>
							<input class="form-control" type="text" name="nom" title="votre nom ne doit pas contenir des caracteres speciaux" id="nom" required="" placeholder="entrez votre nom"><span class="glyphicon glyphicon-user form_icon pull pull-right"></span>
							<p class="avertir" id="alertnom">" Veuillez renseigner votre nom "</p>
						</div>
						<div class="form-group">
							<label for="prenom">Prenoms:</label>
							<input class="form-control " type="text"  title="votre prenom ne doit pas contenir des caracteres speciaux"  name="prenom" id="prenom" required="" placeholder="entrez votre prenom"><span class="glyphicon glyphicon-user form_icon pull pull-right"></span>
							<p class="avertir" id="alertprenom"></p>
						</div>
						<div class="form-group">
							<label for="sexe">Sexe:</label>
							<select name="sexe" class="sexe form-control" id="sexe" required="">
		                   <option selected="" disabled="">Indiquez votre sexe</option>
		                   <option value="Homme">Homme</option>
		                   <option value="Femme">Femme</option>
		              		</select>
		           		</div>
						<div class="form-group">
							<label for="date_naiss">Date de naissance:</label>
							<input class="form-control" type="date" name="date_naiss" id="date_naiss" required="" >
						</div>
						<div class="form-group">
							<label for="telephone" >Numero de telephone:</label>
							<input class="form-control" pattern="[0-9]+" minlength="9" maxlength="15" title="veuillez entrer un numero valide" type="tel" id="telephone" name="telephone" required="" placeholder="+237690755979"><span class="glyphicon glyphicon-earphone form_icon pull pull-right"></span>
							<p class="avertir" id="alertphone"></p>
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input class="form-control" type="email" name="email" id="email" required="" placeholder="entrez votre email"><span class="glyphicon glyphicon-envelope form_icon pull pull-right"></span>
							<p class="avertir" id="alertmail">" Veuillez renseigner votre adresse mail "</p>
						</div>
						<div class="form-group">
							<label for="pwd">Mot de passe:</label>
							<input class="form-control" type="password" minlength="6" maxlength="10" name="password" id="pwd" required="" placeholder="entrez un mot de passe securise"><span class="glyphicon glyphicon-lock form_icon pull pull-right"></span>
							<p class="avertir">" Veuillez renseigner votre Mot de passe. "</p>
						</div>

			      		<div class="form-group">
			      		<label for="pays">Dans quel pays habitez-vous ?</label>
					       <select name="pays" id="pays" class="form-control">
					           <optgroup label="Afrique">
					               <option value="cameroun">Cameroun</option>
					               <option value="tchad">Tchad</option>
					               <option value="congo">Congo</option>
					               <option value="gabon">Gabon</option>
					               <option value="rwanda">Rwanda</option>
					               <option value="RCA">RCA</option>
					           </optgroup>
					           <optgroup label="Amérique">
					               <option value="canada">Canada</option>
					               <option value="etats-unis">Etats-Unis</option>
					           </optgroup>
					           <optgroup label="Asie">
					               <option value="chine">Chine</option>
					               <option value="japon">Japon</option>
					           </optgroup>
					       </select>
					       <p class="avertir" id="alertphone">" Veuillez renseigner le nom de votre pays "</p>
					   </div>
			      	<div class="form-group">
			      		<label for="photo">Photo de profil:</label>
			      		<input type="file" name="photo" id="photo" class="form-control" accept="image/*" onchange="loadFile(event)" required=""><span class="glyphicon glyphicon-picture form_icon pull pull-right"></span>
			      	</div>
			      	<div class="row">
				      	<div class="col-md-offset-5 col-md-4 ">
								 <img id="pp" />
				      	</div>
				    </div>
			      	<div class="row">
				      	<div class="col-md-3 col-xs-12">
				      		<button type="reset" class="btn btn-block btn-danger">
				      			<span class="glyphicon glyphicon-remove pull pull-left"></span>
				      			<strong>ANNULER</strong> 
				      		</button>
				      	</div>
				      	<div class="col-md-push-6 col-md-3 col-xs-12">
				      		<button type="submit" class="btn btn-block btn-success ">
				      			<span class="glyphicon glyphicon-saved pull pull-left"></span>
				      			<strong>ENREGISTRER</strong>
				      		</button>
				      	</div>
			      	</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="Javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

  <script type="text/javascript">
    var loadFile = function(event) {
        var profil = document.getElementById('pp');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
  </script>

  <script type="text/javascript">
  		var	nom = document.getElementById('nom'),
		alertnom = document.getElementById('alertnom');

	nom.addEventListener('focus', function(){
		alertnom.style.opacity='0';
	});

	nom.addEventListener('blur', testnom);
	function testnom(){
	 	if(nom.value.length<3){
	 		alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
	 		alertnom.style.opacity='1';
			return false;
		}
		else if(contient_specialchart(nom.value)){ 
			alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
			alertnom.style.opacity = '1';
			return false;
		}else{
			return true;
		}
	}
	function contient_specialchart(char){
		var special = ['@','!','~','<','>','?',' " '," ' ",'*','(',')','^','%','$','#','&','{','}','[',']',';'];

		for (var i = special.length - 1; i >= 0; i--) { //pour chaque element de special
			for (var id in char) {  // on se rassure qu'il ne soit pas dans char
				if(char[id]==special[i]){
					return true;
				}
			}
		}
		return false;
	}

	// traitement prenom
	var	prenom = document.getElementById('prenom'),
		alertprenom = document.getElementById('alertprenom');

	prenom.addEventListener('focus', function(){
		alertprenom.style.opacity='0';
	});

	prenom.addEventListener('blur', testprenom);  // on controle la valeur laissee dans le champ
	function testprenom(){
	 	if(prenom.value.length<3){  //test de taille de contenu
	 		alertprenom.innerHTML = "Le prenom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
	 		alertprenom.style.opacity='1';   //penser a le faire avec une duree definie. 
			return false;
		}
		else if(contient_specialchart(prenom.value)){  // test de caracteres non indiques
			alertprenom.innerHTML = "Le prenom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
			alertprenom.style.opacity = '1';
			return false;
		}else{
			return true;
		}
	}

	// traitement du phone number 
	var phone = document.getElementById('phone'),
		alertphone = document.getElementById('alertphone');

	phone.addEventListener('focus', function(){	// on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
		alertphone.style.opacity='0';
	});
		
	phone.addEventListener('blur', testphone);
	function testphone(){
		console.log(typeof(phone.value%1));
		console.log(phone.value%1);
		if (phone.value%1!=0){  // on se rassure que le champ ne contient que des chiffres.
			console.log('ici la fonction'); 
			alertphone.innerHTML = "Le numero de telephone ne doit contenir que des chiffres sans espace ni caracteres tels que @ ou % ...";
			alertphone.style.opacity = '1';
			return false;
		}else{
			return true;
		}
		
	}

	// traitement de l'adresse mail 
	var mail = document.getElementById('email');
		alertmail = document.getElementById('alertmail');

	mail.addEventListener('focus', function(){	
		alertmail.style.opacity='0';
	});

	mail.addEventListener('blur', testmail);
	function testmail(){
		var mail_indic='0';
		var mail_format=[],mail_ending=[];
		mail_format=['gmail.com','yahoo.com','yahoo.fr','hotmail.com']; 

		for(var i=0; i<mail.value.length; i++){  
			if(mail.value[i]=='@'){		// si @ est reouve on le garde comme indicateur
				mail_indic=mail.value[i];
			}
		}
		
		if(mail_indic=='@'){         
			mail_ending=mail.value.split('@');
			
			for (var i = 0; i < mail_format.length; i++) {
				console.log(mail_format[i]);
				console.log(mail_format.length);
				if(mail_format[i]==mail_ending[1]){		// on verifie si la partie entree en adresse apres le @ vaut bien l'une des valeurs souhaitee
					return true;
				}
				else if(i==mail_format.length-1 && mail_format[i]!=mail_ending[1]){   // si jusqu'au dernier element des formats on ne trouve rien alors erreur
					alertmail.innerHTML = "Le format de mail attendu est 'eutilisateur@xmail.xxx";
					alertmail.style.opacity='1';
					return false;
				}
			}
		}
		else{ 		// si on n'a pas l'indicateur, l'adresse mail n'est pas valide
			alertmail.innerHTML = "Le format de mail attendu est 'utilisateur@xmail.xxx";
			alertmail.style.opacity = '1';
			return false;
		}
	}



  </script>

</body>
</html>